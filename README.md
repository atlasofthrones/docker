# Atlas-Of-Thrones

An interactive "Game of Thrones" map powered by Leaflet, PostGIS, and Redis.

Visit https://atlasofthrones.com/ to explore the application.

Visit http://blog.patricktriest.com/game-of-thrones-map-node-postgres-redis/ for a tutorial on building the backend, using Node.js, PostGIS, and Redis.

Visit https://blog.patricktriest.com/game-of-thrones-leaflet-webpack/ for part II of the tutorial, which provides a detailed guide to building the frontend webapp using Webpack, Leaflet, and framework-less Javascript components.

Visit https://github.com/triestpa/Atlas-Of-Thrones for application source code

![](https://cdn.patricktriest.com/blog/images/posts/got_map/got_map.jpg)

#### Structure
- `webui/` - The front-end web app container.
- `api/` - The Node.js API server container.
- `db/` - Postgis DB server preloaded with SQL initization script
- `src/` - Files used by `docker-compose` to start everything

#### Run Locally

To start the project locally, simply download or clone the project to your local machine and `docker-compose up -d`.

To stop the project ran locally, execute `docker-compose down`.

Tl;dr:
```shell
git clone https://bitbucket.org/altasofthrones/docker.git \
 && cd docker \
 && docker-compose up -d`
```
#### Access Local Endpoints

- `http://localhost:8080/` - Front-end web app
- `http://localhost:5000/kingdoms` - API server Kingdoms endpoint
- `http://localhost:8000` - Web UI for Postgres DB
